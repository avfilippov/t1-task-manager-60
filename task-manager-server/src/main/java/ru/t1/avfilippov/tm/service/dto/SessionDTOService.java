package ru.t1.avfilippov.tm.service.dto;

import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.repository.dto.ISessionDTORepository;
import ru.t1.avfilippov.tm.api.service.dto.ISessionDTOService;
import ru.t1.avfilippov.tm.dto.model.SessionDTO;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.exception.field.UserIdEmptyException;
import ru.t1.avfilippov.tm.repository.dto.SessionDTORepository;

import javax.persistence.EntityManager;

@Service
@NoArgsConstructor
public final class SessionDTOService extends AbstractUserOwnedDTOService<SessionDTO, SessionDTORepository>
        implements ISessionDTOService {

    @NotNull
    @Autowired
    private ISessionDTORepository repository;

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public SessionDTO add(@Nullable final SessionDTO model) {
        if (model == null) throw new ProjectNotFoundException();
        if (model.getUserId() == null) throw new UserIdEmptyException();
        return repository.add(model.getUserId(), model);
    }

}
