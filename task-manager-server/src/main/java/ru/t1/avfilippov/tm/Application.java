package ru.t1.avfilippov.tm;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.avfilippov.tm.component.Bootstrap;
import ru.t1.avfilippov.tm.configuration.ServerConfiguration;

public final class Application {

    public static void main(@NotNull final String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(ServerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.start();
    }

}
