package ru.t1.avfilippov.tm.service.model;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.t1.avfilippov.tm.api.repository.model.ISessionRepository;
import ru.t1.avfilippov.tm.api.service.model.ISessionService;
import ru.t1.avfilippov.tm.model.Session;
import ru.t1.avfilippov.tm.repository.model.SessionRepository;

@Service
@NoArgsConstructor
public final class SessionService extends AbstractUserOwnedService<Session, SessionRepository>
        implements ISessionService {

    @NotNull
    @Autowired
    private ISessionRepository repository;

}
