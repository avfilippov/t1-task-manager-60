package ru.t1.avfilippov.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.avfilippov.tm.api.repository.dto.IUserDTORepository;
import ru.t1.avfilippov.tm.api.service.IPropertyService;
import ru.t1.avfilippov.tm.api.service.dto.IProjectDTOService;
import ru.t1.avfilippov.tm.api.service.dto.ITaskDTOService;
import ru.t1.avfilippov.tm.api.service.dto.IUserDTOService;
import ru.t1.avfilippov.tm.dto.model.UserDTO;
import ru.t1.avfilippov.tm.enumerated.Role;
import ru.t1.avfilippov.tm.exception.entity.UserNotFoundException;
import ru.t1.avfilippov.tm.exception.field.*;
import ru.t1.avfilippov.tm.exception.user.ExistsEmailException;
import ru.t1.avfilippov.tm.exception.user.ExistsLoginException;
import ru.t1.avfilippov.tm.repository.dto.UserDTORepository;
import ru.t1.avfilippov.tm.util.HashUtil;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class UserDTOService extends AbstractDTOService<UserDTO, UserDTORepository>
        implements IUserDTOService {

    @NotNull
    @Autowired
    private IProjectDTOService projectService;

    @NotNull
    @Autowired
    private ITaskDTOService taskService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private IUserDTORepository repository;

    @Override
    @NotNull
    @Transactional
    public UserDTO create(@Nullable final String login, @Nullable final String password) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return repository.add(user);
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        return repository.add(user);
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        return repository.add(user);
    }

    @Override
    @NotNull
    @Transactional
    public UserDTO create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email,
            @Nullable final String lastName,
            @Nullable final String firstName,
            @Nullable final String middleName
    ) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (isLoginExists(login)) throw new ExistsLoginException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (firstName == null || firstName.isEmpty()) throw new NameEmptyException();
        if (lastName == null || lastName.isEmpty()) throw new NameEmptyException();
        if (isEmailExists(email)) throw new ExistsEmailException();
        @NotNull UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setLastName(lastName);
        user.setFirstName(firstName);
        user.setEmail(email);
        if (middleName != null) user.setMiddleName(middleName);
        return repository.add(user);
    }

    @Override
    @Nullable
    public UserDTO findByEmail(@Nullable final String email) {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return repository.findByEmail(email);
    }

    @Override
    @Nullable
    public UserDTO findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return repository.findByLogin(login);
    }

    @Override
    public Boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Override
    public Boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(true);
        repository.update(user);
    }

    @Override
    @Transactional
    public void removeByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        @NotNull final String userId = user.getId();
        projectService.clear(userId);
        taskService.clear(userId);
        remove(user);
    }

    @Override
    @Transactional
    public void setPassword(@Nullable final String id, @Nullable final String password) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        repository.update(user);
    }

    @Override
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        repository.update(user);
    }

    @Override
    @Transactional
    public void updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (firstName == null || firstName.isEmpty()) throw new NameEmptyException();
        if (lastName == null || lastName.isEmpty()) throw new NameEmptyException();
        @Nullable final UserDTO user = findOneById(id);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setMiddleName(middleName);
        repository.update(user);
    }

}
