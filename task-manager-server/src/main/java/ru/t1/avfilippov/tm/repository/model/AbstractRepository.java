package ru.t1.avfilippov.tm.repository.model;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.avfilippov.tm.api.repository.model.IRepository;
import ru.t1.avfilippov.tm.comparator.CreatedComparator;
import ru.t1.avfilippov.tm.comparator.StatusComparator;
import ru.t1.avfilippov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.Comparator;
import java.util.List;

@Repository
@Scope("prototype")
public abstract class AbstractRepository<M extends AbstractModel> implements IRepository<M> {

    @Getter
    @NotNull
    @PersistenceContext
    protected EntityManager entityManager;

    @NotNull
    protected abstract Class<M> getClazz();

    @NotNull
    protected String getSortType(@NotNull final Comparator comparator) {
        if (comparator == CreatedComparator.INSTANCE) return "created";
        if (comparator == StatusComparator.INSTANCE) return "status";
        else return "name";
    }

    @NotNull
    @Override
    public M add(@NotNull final M model) {
        entityManager.persist(model);
        return model;
    }

    @Override
    public void clear() {
        @NotNull final String jpql = "DELETE FROM " + getClazz().getSimpleName() + " m";
        entityManager.createQuery(jpql).executeUpdate();
    }

    @Override
    public boolean existsById(@NotNull final String id) {
        return findOneById(id) != null;
    }

    @Override
    @Nullable
    public List<M> findAll() {
        @NotNull final String jpql = "SELECT m FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, getClazz()).getResultList();
    }

    @Override
    @Nullable
    public List<M> findAll(@Nullable final Comparator comparator) {
        @NotNull final CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        @NotNull final CriteriaQuery<M> criteriaQuery = criteriaBuilder.createQuery(getClazz());
        @NotNull final Root<M> from = criteriaQuery.from(getClazz());
        criteriaQuery.select(from);
        criteriaQuery.orderBy(criteriaBuilder.asc(from.get(getSortType(comparator))));
        return entityManager.createQuery(criteriaQuery).getResultList();
    }

    @Override
    @Nullable
    public M findOneById(@NotNull final String id) {
        return entityManager.find(getClazz(), id);
    }

    @Override
    public int getSize() {
        @NotNull final String jpql = "SELECT COUNT(m) FROM " + getClazz().getSimpleName() + " m";
        return entityManager.createQuery(jpql, Long.class).getSingleResult().intValue();
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

}
