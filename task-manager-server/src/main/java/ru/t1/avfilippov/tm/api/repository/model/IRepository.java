package ru.t1.avfilippov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.model.AbstractModel;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public interface IRepository<M extends AbstractModel> {

    M add(@NotNull M model);

    void clear();

    boolean existsById(@NotNull String id);

    @Nullable
    List<M> findAll();

    @Nullable
    List<M> findAll(@Nullable Comparator comparator);

    @Nullable
    M findOneById(@NotNull String id);

    int getSize();

    void remove(@NotNull M model);

    void update(@NotNull M model);

}
