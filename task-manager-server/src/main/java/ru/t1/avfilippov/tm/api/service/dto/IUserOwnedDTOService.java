package ru.t1.avfilippov.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.t1.avfilippov.tm.api.repository.dto.IUserOwnedDTORepository;
import ru.t1.avfilippov.tm.dto.model.AbstractUserOwnedModel;
import ru.t1.avfilippov.tm.enumerated.Sort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModel> extends IUserOwnedDTORepository<M> {

    @Nullable
    List<M> findAll(@Nullable String userId, @Nullable Sort sort);

    void removeById(@Nullable String userId, @Nullable String id);

}
