package ru.t1.avfilippov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ProjectShowByIdRequest;
import ru.t1.avfilippov.tm.dto.response.ProjectShowByIdResponse;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.exception.entity.ProjectNotFoundException;
import ru.t1.avfilippov.tm.dto.model.ProjectDTO;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class ProjectShowByIdListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "show project by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-show-by-id";
    }

    @Override
    @EventListener(condition = "@projectShowByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[SHOW PROJECT BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull final ProjectShowByIdRequest request = new ProjectShowByIdRequest(getToken());
        request.setProjectId(id);
        @Nullable final ProjectShowByIdResponse response = projectEndpoint.showProjectById(request);
        if (response.getProject() == null) throw new ProjectNotFoundException();
        @Nullable final ProjectDTO project = response.getProject();
        showProject(project);
    }

}
