package ru.t1.avfilippov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ServerVersionRequest;
import ru.t1.avfilippov.tm.dto.response.ServerVersionResponse;
import ru.t1.avfilippov.tm.event.ConsoleEvent;

@Component
public final class ApplicationVersionListener extends AbstractSystemListener {

    @NotNull
    @Override
    public String getArgument() {
        return "-v";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show application version";
    }

    @NotNull
    @Override
    public String getName() {
        return "version";
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[VERSION]");
        System.out.print("Client: ");
        System.out.println(propertyService.getApplicationVersion());
        System.out.print("Server: ");
        @Nullable final ServerVersionRequest request = new ServerVersionRequest(getToken());
        @Nullable final ServerVersionResponse response = systemEndpoint.getVersion(request);
        System.out.println(response.getVersion());
    }

}
