package ru.t1.avfilippov.tm.listener.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.TaskCompleteByIdRequest;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.util.TerminalUtil;

@Component
public final class TaskCompleteByIdListener extends AbstractTaskListener {

    @NotNull
    @Override
    public String getDescription() {
        return "complete task by id";
    }

    @NotNull
    @Override
    public String getName() {
        return "task-complete-by-id";
    }

    @Override
    @EventListener(condition = "@taskCompleteByIdListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) throws Exception {
        System.out.println("[COMPLETE TASK BY ID]");
        System.out.println("ENTER ID:");
        @Nullable final String id = TerminalUtil.nextLine();
        @NotNull TaskCompleteByIdRequest request = new TaskCompleteByIdRequest(getToken());
        request.setTaskId(id);
        taskEndpoint.completeTaskById(request);
    }

}
