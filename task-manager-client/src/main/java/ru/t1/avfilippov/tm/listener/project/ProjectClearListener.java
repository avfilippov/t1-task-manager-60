package ru.t1.avfilippov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.dto.request.ProjectClearRequest;
import ru.t1.avfilippov.tm.event.ConsoleEvent;

@Component
public final class ProjectClearListener extends AbstractProjectListener {

    @NotNull
    @Override
    public String getDescription() {
        return "remove all projects";
    }

    @NotNull
    @Override
    public String getName() {
        return "project-clear";
    }

    @Override
    @EventListener(condition = "@projectClearListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[CLEAR PROJECTS]");
        @NotNull ProjectClearRequest request = new ProjectClearRequest(getToken());
        projectEndpoint.clearProject(request);
    }

}
