package ru.t1.avfilippov.tm.listener.system;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.api.model.ICommand;
import ru.t1.avfilippov.tm.event.ConsoleEvent;
import ru.t1.avfilippov.tm.listener.AbstractListener;

import java.util.Arrays;
import java.util.Collection;

@Component
public final class ArgumentListListener extends AbstractSystemListener {

    @Getter
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Override
    public String getArgument() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "show arguments list";
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @Override
    @EventListener(condition = "@applicationVersionListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        @NotNull final Collection<AbstractListener> commands = Arrays.asList(listeners);
        for (@NotNull final ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

}
