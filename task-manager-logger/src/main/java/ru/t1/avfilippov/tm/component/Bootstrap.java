package ru.t1.avfilippov.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.avfilippov.tm.Application;
import ru.t1.avfilippov.tm.listener.EntityListener;

import javax.jms.*;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    private ConnectionFactory connectionFactory;

    @NotNull
    @Autowired
    private EntityListener entityListener;

    @SneakyThrows
    public void startLogger() {
        @NotNull final Connection connection = connectionFactory.createConnection();
        connection.start();
        @NotNull final Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        @NotNull final Queue destination = session.createQueue(Application.QUEUE);
        @NotNull final MessageConsumer messageConsumer = session.createConsumer(destination);
        messageConsumer.setMessageListener(entityListener);
    }

}
