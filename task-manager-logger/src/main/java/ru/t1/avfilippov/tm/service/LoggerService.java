package ru.t1.avfilippov.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import lombok.SneakyThrows;
import org.bson.Document;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;

import java.util.LinkedHashMap;
import java.util.Map;

@Service
public final class LoggerService {

    @NotNull
    private final ObjectMapper objectMapper = new ObjectMapper();

    @NotNull
    private final MongoClient mongoClient = new MongoClient("172.0.0.1", 27017);

    @NotNull
    private final MongoDatabase mongoDatabase = mongoClient.getDatabase("tm_logger");

    @SneakyThrows
    public void log(final String json) {
        @NotNull final Map<String, Object> event = objectMapper.readValue(json, LinkedHashMap.class);
        @NotNull final String collectionName = event.get("table").toString();
        if (mongoDatabase.getCollection(collectionName) == null)
            mongoDatabase.createCollection(collectionName);
        @NotNull final MongoCollection<Document> collection = mongoDatabase.getCollection(collectionName);
        collection.insertOne(new Document(event));
    }

}
