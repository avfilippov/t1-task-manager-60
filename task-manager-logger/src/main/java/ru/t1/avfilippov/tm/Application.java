package ru.t1.avfilippov.tm;

import lombok.SneakyThrows;
import org.apache.activemq.ActiveMQConnection;
import org.jetbrains.annotations.NotNull;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import ru.t1.avfilippov.tm.component.Bootstrap;
import ru.t1.avfilippov.tm.configuration.LoggerConfiguration;

public final class Application {

    @NotNull
    public static final String QUEUE = "LOGGER";

    @SneakyThrows
    public static void main(String[] args) {
        @NotNull final ApplicationContext context = new AnnotationConfigApplicationContext(LoggerConfiguration.class);
        @NotNull final Bootstrap bootstrap = context.getBean(Bootstrap.class);
        bootstrap.startLogger();
    }

}
